# 这是一个discuz论坛app的前后端项目

## 演示app

[apk下载地址](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-03752401-f782-4c80-b46b-b8b69bd129f8/5c63103d-688b-4537-97ea-3b56fd22b983.apk)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-03752401-f782-4c80-b46b-b8b69bd129f8/4a3d6c26-1af5-4679-b0fb-29434fa0f927.png)

## 演示图

![](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/6d7cbed0-2daf-11ec-ba8d-6547c748fac0_0.jpg?image_process=quality,q_70&v=1644571986)

![](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/6d7cbed0-2daf-11ec-ba8d-6547c748fac0_1.jpg?image_process=quality,q_70&v=1644572166)

![](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/6d7cbed0-2daf-11ec-ba8d-6547c748fac0_3.jpg?image_process=quality,q_70&v=1644572175)

## 目前实现的功能

1. 登录注册
2. 帖子列表
3. 版块列表
4. 回帖
5. 贴内图片查看
6. 收藏取消帖子中
7. 搜索
8. 扫码
9. 幻灯片轮播


## 目前所有页面均为 nvue

	建议设为纯nvue或fast启动模式，manifest.json-App常用其他设置-选中fast启动模式。提升性能


##  直接运行到手机即可调试

如果无法登陆或注册，请查看 ucenter通信是否正常

## 后端api接口文件下载及使用方法


**接口文件 api.php 在本插件目录下 phpapi/api.php 中**

>
> 在网站根目录下创建 app文件夹，然后将 api.php 放在其中
> 
> 然后修改 app源码/common/config.php 中 apidomain 为 你的网址
> 
> 扣：21贰4肆55零76
>


## 幻灯片使用方法

在接口文件 api.php 同目录下放置一个 banner.txt文件，内容为

src:https://bossaudioandcomic-1252317822.image.myqcloud.com/activity/document/d080c591ff088fab85044dde7b0efc0b.jpg

tit:图片1

url:https://zzk.cnblogs.com/s/blogpost?Keywords=pyqt&DateTimeRange=OneYear


则显示一张幻灯片,如果要显示多张，则设置多组上述内容，如图
![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-03752401-f782-4c80-b46b-b8b69bd129f8/33d987fe-ef41-49af-a43d-78ed69ea1131.png)


url如果为帖子的tid数值，则直接app内打开帖子




## 市场下载地址

[https://ext.dcloud.net.cn/plugin?id=6426](https://ext.dcloud.net.cn/plugin?id=6426)