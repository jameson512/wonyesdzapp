// /common/http.api.js
import config from '@/common/config.js'

// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等
const install = (Vue, vm) => {

	// 登录注册相关

	let getForums = (params = {}) => vm.$u.post('/app/api.php?action=getForums', params);
	let login2reg = (params = {}) => vm.$u.post('/app/api.php?action=login2reg', params);

	let getMyFavores = (params = {}) => vm.$u.post('/app/api.php?action=getMyFavores', params);
	let removeFavore = (params = {}) => vm.$u.post('/app/api.php?action=removeFavore', params);
	let getMyPost = (params = {}) => vm.$u.post('/app/api.php?action=getMyPost', params);
	let getMyThread = (params = {}) => vm.$u.post('/app/api.php?action=getMyThread', params);
	let updateUserinfo = (params = {}) => vm.$u.post('/app/api.php?action=updateUserinfo', params);
	let getSearch = (params = {}) => vm.$u.post('/app/api.php?action=getSearch', params);
	let getPostByTid = (params = {}) => vm.$u.post('/app/api.php?action=getPostByTid', params);
	let getBodyByTid = (params = {}) => vm.$u.post('/app/api.php?action=getBodyByTid', params);
	let postReply = (params = {}) => vm.$u.post('/app/api.php?action=postReply', params);
	let setFavore = (params = {}) => vm.$u.post('/app/api.php?action=setFavore', params);
	
	let getbanner=(params={})=>vm.$u.post('/app/api.php?action=getbanner',params)




	// 上传头像
	let uploadAvatar = (params = {}) => {
		return new Promise((resolve, reject) => {
			let options = {
				url: config.apidomain + '/app/api.php?action=uploadAvatar', //仅为示例，非真实的接口地址
				filePath: params['image'],
				name: 'img',
				formData: params,
				success: (uploadFileRes) => {
					console.log(uploadFileRes);
					resolve(uploadFileRes.data)
				},
				fail: (err) => {
					console.log(err);
					reject(uploadFileRes.data)
				}
			}
			uni.uploadFile(options);
		});
	}

	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		getForums,
		login2reg,
		getMyFavores,
		removeFavore,
		getMyPost,
		getMyThread,
		updateUserinfo,
		getSearch,
		uploadAvatar,
		getPostByTid,
		getBodyByTid,
		postReply,
		setFavore,
		getbanner
	};
}

export default {
	install
}
