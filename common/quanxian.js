    /**  
			     * 获取系统权限  
			     * @param {Object} permission       权限标识  
			     *      CAMERA: (String 类型 )访问摄像头权限         用于调用摄像头（plus.camera.* plus.barcode.*）  
			     *      CONTACTS: (String 类型 )访问系统联系人权限     用于访问（读、写）系统通讯录（plus.gallery.*）  
			     *      GALLERY: (String 类型 )访问系统相册权限       用于访问（读、写）系统相册（plus.gallery.*）  
			     *      LOCATION: (String 类型 )定位权限              用于获取当前用户位置信息（plus.geolocation.*）  
			     *      NOTIFITION: (String 类型 )消息通知权限      用于接收系统消息通知（plus.push.*）  
			     *      RECORD: (String 类型 )录音权限                用于进行本地录音操作（plus.audio.AudioRecorder）  
			     *      SHORTCUT: (String 类型 )创建桌面快捷方式权限    用于在系统桌面创建快捷方式图标（plus.navigator.createShortcut）  
			     *   
			     * @param {Object} successCallBack  成功回调  
			     * @param {Object} errorCallBack    失败回调  
			     */  
export default	function (permissionIdentity, successCallBack, errorCallBack){  
			        //权限标识转换成大写  
			        var permissionIdentity = permissionIdentity.toUpperCase();  
			        //获取检测权限的状态  
			        var checkResult = plus.navigator.checkPermission(permissionIdentity);  
			        //权限状态是否正常  
			        var permissionStatusOk = false;  
			        //权限中文名称  
			        var permissionName = '';  
			        //对应 andorid 的具体权限  
			        var androidPermission = '';  
			        //获取权限中文意思与对应 android 系统的权限字符串  
			        switch (permissionIdentity) {  
			            case 'CAMERA':  
			                permissionName = '摄像头';  
			                androidPermission = 'android.permission.CAMERA';  
			                break;  
			            case 'CONTACTS':  
			                permissionName = '系统联系人';  
			                androidPermission = 'android.permission.READ_CONTACTS'  
			                break;  
			            case 'GALLERY':  
			                permissionName = '系统相册';  
			                androidPermission = 'android.permission.READ_EXTERNAL_STORAGE';  
			                break;  
			            case 'LOCATION':  
			                permissionName = '定位';  
			                androidPermission = 'android.permission.ACCESS_COARSE_LOCATION';  
			                break;  
			            case 'NOTIFITION':  
			                permissionName = '消息通知';  
			                androidPermission = '消息通知';  
			                break;  
			            case 'RECORD':  
			                permissionName = '录音';  
			                androidPermission = 'android.permission.RECORD_AUDIO';  
			                break;  
			            case 'SHORTCUT':  
			                permissionName = '创建桌面快捷方式';  
			                androidPermission = 'com.android.launcher.permission.INSTALL_SHORTCUT';  
			                break;  
			            default:  
			                permissionName = '未知';  
			                androidPermission = '未知';  
			                break;  
			        }  
			
			        //判断检查权限的结果  
			        switch (checkResult) {  
			            case 'authorized':  
			                //正常的  
			                permissionStatusOk = true  
			                break;  
			            case 'denied':  
			                //表示程序已被用户拒绝使用此权限，如果是拒绝的就再次提示用户打开确认提示框  
			                //如果有该权限但是没有打开不进行操作还是会去申请或手动打开  
			                // console.log('已关闭' + permissionName + '权限')  
			                // errorCallBack('已关闭' + permissionName + '权限');  
			                // return  
			                break;  
			            case 'undetermined':  
			                // 表示程序未确定是否可使用此权限，此时调用对应的API时系统会弹出提示框让用户确认  
			                // this.requestPermissions(androidPermission, permissionName, successCallBack, errorCallBack)  
			                // errorCallBack('未确定' + permissionName + '权限');  
			                // return  
			                break;  
			            case 'unknown':  
			                errorCallBack('无法查询' + permissionName + '权限');  
			                return  
			                break;  
			            default:  
			                errorCallBack('不支持' + permissionName + '权限');  
			                return  
			                break;  
			        }  
			
			        //如果权限是正常的执行成功回调  
			        if (permissionStatusOk) {  
			            successCallBack()  
			        } else {  
			            //如果不正常，如果是 andorid 系统，就动态申请权限  
			            if (plus.os.name == 'Android') {  
			                //动态申请权限  
			                plus.android.requestPermissions([androidPermission], function(e) {  
			                    if (e.deniedAlways.length > 0) {  
			                        //权限被永久拒绝  
			                        // 弹出提示框解释为何需要定位权限，引导用户打开设置页面开启  
			                        errorCallBack(permissionName + ' 权限被永久拒绝，请到设置权限里找到应用手动开启权限，否则将不能使用此功能。')  
			                        // console.log('Always Denied!!! ' + e.deniedAlways.toString());  
			                    }  
			                    if (e.deniedPresent.length > 0) {  
			                        //权限被临时拒绝  
			                        // 弹出提示框解释为何需要定位权限，可再次调用plus.android.requestPermissions申请权限  
			                        errorCallBack('拒绝开启 ' + permissionName + ' 权限，将不能使用此功能！')  
			                        // console.log('Present Denied!!! ' + e.deniedPresent.toString());  
			                    }  
			                    if (e.granted.length > 0) {  
			                        //权限被允许  
			                        //调用依赖获取定位权限的代码  
			                        successCallBack()  
			                        // console.log('Granted!!! ' + e.granted.toString());  
			                    }  
			                }, function(e) {  
			                    errorCallBack('请求 ' + permissionName + ' 权限失败，' + +JSON.stringify(e))  
			                    // console.log('Request Permissions error:' + JSON.stringify(e));  
			                })  
			            } else if (plus.os.name == 'iOS') {  
			                //ios ,第一次使用目的权限时，应用的权限列表里是不存在的，所以先默认执行一下成功回调，打开要使用的操作，比如 plus.camera  
			                //这时系统会提示是否打开相应的权限，如果拒绝也没关系，因为应用的权限列表里已经存在该权限了，下次再调用相应权限时，就会  
			                //走 else 里的流程，会给用户提示，并且跳转到应该的权限页面，让用户手动打开。  
			                if (checkResult == 'undetermined') {  
			                    //调用依赖获取定位权限的代码  
			                    successCallBack(true)  
			                } else {  
			                    //如果是 ios 系统，ios 没有动态申请操作，所以提示用户去设置页面手动打开  
			                    mui.confirm(permissionName + ' 权限没有开启，是否去开启？', '提醒', ['取消', '确认'], function(e) {  
			                        //取消  
			                        if (e.index == 0) {  
			                            errorCallBack('拒绝开启 ' + permissionName + ' 权限，将不能使用此功能！')  
			                        } else if (e.index == 1) {  
			                            //确认，打开当前应用权限设置页面  
			                            var UIApplication = plus.ios.import('UIApplication');  
			                            var application2 = UIApplication.sharedApplication();  
			                            var NSURL2 = plus.ios.import('NSURL');  
			                            // var setting2 = NSURL2.URLWithString("prefs:root=LOCATION_SERVICES");                             
			                            var setting2 = NSURL2.URLWithString('app-settings:');  
			                            application2.openURL(setting2);  
			
			                            plus.ios.deleteObject(setting2);  
			                            plus.ios.deleteObject(NSURL2);  
			                            plus.ios.deleteObject(application2)  
			                        }  
			                    }, 'div')  
			                }  
			            }  
			        }  
			}